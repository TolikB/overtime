1.times do |user|
  User.create!  email: "admin@example.com",
                first_name: "Jon",
                last_name: "Snow",
                password: "qwerty2016",
                password_confirmation: "qwerty2016"
end
100.times do |post|
  Post.create!  date: Date.today,
                rationale: "#{post} rationale content.",
                user_id: 1
end

puts "*".center(50, "*")
puts "1 user created.".center(50)
puts "*".center(50, "*")

puts "*".center(50, "*")
puts "100 posts created.".center(50)
puts "*".center(50, "*")
